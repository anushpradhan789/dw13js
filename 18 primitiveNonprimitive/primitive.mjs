// primitive => number, string ,boolean, undefined ,null
//memory allocation 
//if let is used memory is allocated for that
// === looks for value in primitive
let a=1
let b=a
let c=1
a=2
console.log("primitive")
console.log(a)
console.log(b)
console.log(c)
console.log(a===c)
console.log(a===b)

console.log("-----------------------------------------------------------------")
//non primitive data type => array, object , error, date
//memory allocation 
// before allocating memory it checks whether the variable is copy of another variable
// if the variable is copy of another , it will share memory
// === looks for memory allocation is non-primitive 
let ar1=[1,2,3]
let ar2=ar1
let ar3=[1,2,3]
ar1.push(5)
console.log("non-primitive")
console.log(ar1)
console.log(ar2)
console.log(ar3)
console.log(ar3===ar1)
console.log(ar1===ar2)