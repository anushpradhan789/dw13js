{//parent
    let a=1
    let b=1
    {//child
        let a=2
        let  c=3
        console.log(a)
        console.log(b)
    }
    console.log(a)
    // console.log(c) //error
}
/* 
parent
a=1
b=2


child
a=2
b=1 called from parent
*/